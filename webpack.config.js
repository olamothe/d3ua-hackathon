'use strict';

module.exports = {
  entry: {
    'd3ua': require('path').resolve('./src/Index.ts')
  },
  resolve: {
    extensions: ['', '.ts', '.js']
  },
  output: {
    path: require('path').resolve('./bin/js'),
    filename: '[name].js',
    publicPath: '/js/'
  },
  module: {
    loaders: [
      {test: /\.ts$/, loader: 'ts-loader'}
    ]
  }
}
