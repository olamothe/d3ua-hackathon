/// <reference path='Topojson.d.ts' />
import d3 = require('d3');
import topojson = require('topojson');
import {landData} from './Landdata';
import {IApiData, UAApi, ILocalization} from './UAApi';
import {GoogleApi} from './GoogleApi';
import {Color} from './Color';

const FRONT_LAND_MASS_COLOR = '#263248';
const BACK_LAND_MASS_COLOR = '#7E8AA2';
const GRID_COLOR = 'rgba(255,255,255,.5)';

const MAX_DOMAIN = 1000;
const MAX_RANGE = 10;
const SPEED_GLOW = 10;

interface IGeometry {
  speed: number;
  radius: number;
  color: string;
}


export class Globe {
  private start = Date.now();
  private rScale = d3.scale.sqrt();
  private projection: d3.geo.Projection;
  private path: d3.geo.Path;
  private graticule: d3.geo.Graticule;
  private canvas: d3.Selection<HTMLCanvasElement>;
  private context: CanvasRenderingContext2D;
  private land: any;
  private grid: any;
  private googleApi: GoogleApi;
  private localeCache = {};

  constructor(public width: number, public height: number, public speed = -2e-2, public url = 'https://m5s2v35gke.execute-api.us-east-1.amazonaws.com/prod/LiveUsage') {
    this.googleApi = new GoogleApi();
    this.setupProjection();
    this.setupCanvas();
    this.setupPath();
    this.setupData();

    d3.timer(()=> {
      new UAApi(this.url).get().then((data)=> {
        this.draw(data);
      })
    })
  }


  private setupProjection() {
    this.projection = d3.geo.orthographic()
                        .scale(this.width / 2.1)
                        .translate([this.width / 2, this.height / 2])
                        .precision(0.5);

    this.graticule = d3.geo.graticule();
  }

  private setupCanvas() {
    this.canvas = d3.select('#globe')
                    .attr('width', this.width)
                    .attr('height', this.height);

    this.context = (<HTMLCanvasElement>this.canvas.node()).getContext('2d');
  }

  private setupPath() {
    this.path = d3.geo.path()
                  .projection(this.projection)
                  .pointRadius(this.pointRadius.bind(this))
                  .context(this.context);
  }

  private pointRadius(d) {
    return d.properties ? this.rScale(d.properties.radius) : MAX_DOMAIN / 2;
  }

  private setupData() {
    this.rScale.domain([0, MAX_DOMAIN]);
    this.rScale.range([0, MAX_RANGE]);

    this.land = topojson.feature(landData, landData.objects.land);
    this.grid = this.graticule();
  }

  private draw(data: IApiData[]) {
    this.context.clearRect(0, 0, this.width, this.height);
    this.projection.rotate([this.speed * (Date.now() - this.start), -15, 0]).clipAngle(90)


    this.drawSphere();
    this.drawFrontLand();
    this.drawGrid();

    this.projection.clipAngle(90);

    this.drawBackLand();
    this.drawPoints(data)
  }

  private drawSphere() {
    this.context.beginPath();
    this.path({type: 'Sphere'});
    this.context.lineWidth = 3;
    this.context.strokeStyle = '#000';
    this.context.stroke();
    this.context.fillStyle = '#000';
    this.context.fill();
  }

  private drawFrontLand() {
    this.projection.clipAngle(180);
    this.context.fillStyle = FRONT_LAND_MASS_COLOR;
    this.context.beginPath();
    this.path(this.land);
    this.context.fill();
  }

  private drawGrid() {
    this.context.beginPath();
    this.path(this.grid);
    this.context.lineWidth = .5;
    this.context.strokeStyle = GRID_COLOR;
    this.context.stroke();
  }

  private drawBackLand() {
    this.context.beginPath();
    this.path(this.land);
    this.context.fillStyle = BACK_LAND_MASS_COLOR;
    this.context.fill();
    this.context.lineWidth = .5;
    this.context.strokeStyle = '#000';
    this.context.stroke();
  }

  private drawPoints(points) {


    points.forEach((point)=> {
      if (point.localizations) {

        point.localizations.forEach((localization: ILocalization)=> {
          let key = this.getKey(localization.city, localization.region, localization.country);

          if (this.localeCache[key]) {
            this.context.beginPath();
            this.context.fillStyle = this.localeCache[key].properties.color;

            this.path(this.localeCache[key]);
            this.context.fill();

          } else {
            this.googleApi.get(key).then((latLon)=> {
              if (latLon) {
                this.localeCache[key] = this.latLonToGeom(latLon);
              }
            })
          }
        })
      }
    });
  }

  private handleCircleSpeedAndRadius(geometry: IGeometry) {
    geometry.radius += geometry.speed;
    if (geometry.radius > MAX_DOMAIN || geometry.radius < 0) {
      geometry.speed *= -1;
    }

    if (geometry.radius > MAX_DOMAIN) {
      geometry.radius = MAX_DOMAIN;
    }

    if (geometry.radius < 0) {
      geometry.radius = 0;
    }

    return geometry;
  }

  private latLonToGeom(latLon) {
    let color = new Color().get();
    return {
      type: 'Feature',
      properties: <IGeometry>{
        radius: Math.floor(Math.random() * MAX_DOMAIN),
        speed: SPEED_GLOW,
        color: `rgba(${color.red},${color.green},${color.blue},0.8)`
      },
      geometry: {type: 'Point', coordinates: [latLon.lng, latLon.lat]}
    }
  }

  private getKey(city?: string, region?: string, country?: string) {
    let params = [];
    if (city != 'NOT_AVAILABLE') {
      params.push(city);
    }
    if (region != 'NOT_AVAILABLE') {
      params.push(region);
    }
    if (country != 'NOT_AVAILABLE') {
      params.push(country);
    }

    let key = `${params.join(', ')}`;
    return key;
  }
}
