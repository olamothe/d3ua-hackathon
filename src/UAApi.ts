export interface  IApiData {
  totalCount: string;
  searchCount: string;
  clickCount: string;
  customCount: string;
  viewCount: string;
  timestamp: string;
  localizations: ILocalization[];
}

export interface ILocalization {
  city: string;
  country: string;
  region: string;
}


export class UAApi {
  static lastData: IApiData[];
  static started = false;

  public constructor(public url: string) {

    if (!UAApi.started) {
      setInterval(()=> {
        this.getRegularly();
      }, 1000);
      UAApi.started = true;
    }
  }

  public get(): Promise<IApiData[]> {
    if (UAApi.lastData) {
      return new Promise((resolve)=> {
        resolve(UAApi.lastData)
      })
    } else {

      return this.getRegularly();
    }
  }

  private getRegularly() {
    return this.getFromApi().then((data)=> {
      UAApi.lastData = data;
      return UAApi.lastData;
    });
  }

  private getFromApi() {
    return fetch(this.url)
        .then((response)=> {
          return response.json();
        })
        .then((data: IApiData[])=> {
          return data;
        })
  }
}


