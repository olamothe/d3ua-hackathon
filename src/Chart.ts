import {SmoothieChart, TimeSeries} from 'smoothie';
import {IApiData, UAApi} from './UAApi';

export class Chart {
  public canvas: HTMLCanvasElement;
  public smoothieChart: SmoothieChart;
  public totalCountSeries: TimeSeries;
  public searchCountSeries: TimeSeries;
  public clickCountSeries: TimeSeries;
  public customCountSeries: TimeSeries;
  public viewCountSeries: TimeSeries;

  public totalCountEl: HTMLElement;
  public clickCountEl: HTMLElement;
  public searchCountEl: HTMLElement;
  public customCountEl: HTMLElement;
  public viewCountEl: HTMLElement;

  public constructor(public width: number, public height: number, public url = 'https://m5s2v35gke.execute-api.us-east-1.amazonaws.com/prod/LiveUsage') {
    this.canvas = <HTMLCanvasElement>document.getElementById('chart');
    this.canvas.width = this.width;
    this.canvas.height = this.height;
    this.smoothieChart = new SmoothieChart({
      millisPerPixel: 50,
      grid: {
        strokeStyle: 'transparent',
        borderVisible: false
      },
      labels: {
        fontSize: 16,
        precision: 0
      }
    });
    this.totalCountSeries = new TimeSeries();
    this.searchCountSeries = new TimeSeries();
    this.clickCountSeries = new TimeSeries();
    this.customCountSeries = new TimeSeries();
    this.viewCountSeries = new TimeSeries();

    this.smoothieChart.streamTo(this.canvas, 12000);
    this.smoothieChart.addTimeSeries(this.totalCountSeries, {
      lineWidth: 2,
      strokeStyle: '#176DF7',
      fillStyle: 'rgba(23,109,247,0.50)'
    });
    this.smoothieChart.addTimeSeries(this.searchCountSeries, {
      lineWidth: 2,
      strokeStyle: '#DE2A35',
      fillStyle: 'rgba(222,42,53,0.50)'
    });
    this.smoothieChart.addTimeSeries(this.customCountSeries, {
      lineWidth: 2,
      strokeStyle: '#AD41F0',
      fillStyle: 'rgba(173,65,240,0.50)'
    });
    this.smoothieChart.addTimeSeries(this.clickCountSeries, {
      lineWidth: 2,
      strokeStyle: '#1EC8EB',
      fillStyle: 'rgba(30,200,235,0.50)'
    })
    this.smoothieChart.addTimeSeries(this.viewCountSeries, {
      lineWidth: 2,
      strokeStyle: '#FFAF5A',
      fillStyle: 'rgba(255,175,90,0.50)'
    });

    this.totalCountEl = document.getElementById('totalCount');
    this.clickCountEl = document.getElementById('clickCount');
    this.searchCountEl = document.getElementById('searchCount');
    this.customCountEl = document.getElementById('customCount');
    this.viewCountEl = document.getElementById('viewCount');

    setInterval(this.fetch.bind(this), 1000);
  }

  public fetch() {
    new UAApi(this.url).get().then((data: IApiData[])=> {
      this.draw(data);
    })
  }

  public draw(data: IApiData[]) {
    data.forEach((point)=> {
      if (point.clickCount) {
        let timestamp = parseInt(point.timestamp) * 1000;
        this.totalCountSeries.append(timestamp, parseInt(point.totalCount));
        this.searchCountSeries.append(timestamp, parseInt(point.searchCount));
        this.customCountSeries.append(timestamp, parseInt(point.customCount));
        this.clickCountSeries.append(timestamp, parseInt(point.clickCount));
        this.viewCountSeries.append(timestamp, parseInt(point.viewCount));

        this.totalCountEl.textContent = point.totalCount;
        this.searchCountEl.textContent = point.searchCount;
        this.clickCountEl.textContent = point.clickCount;
        this.customCountEl.textContent = point.customCount;
        this.viewCountEl.textContent = point.viewCount;
      }
    })
  }
}
