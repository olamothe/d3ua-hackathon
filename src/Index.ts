import {Globe} from './Globe';
import {Chart} from './Chart';

let winWidth = window.innerWidth;
let winHeight = window.innerHeight;

let globeSize = winHeight - 100;

new Globe(globeSize, globeSize);
new Chart(winWidth, winHeight - globeSize);
