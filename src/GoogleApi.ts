const url = 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCZnMDBSRtQ4V1ia0AJA5kW18buNIo5oRI&address=';


export class GoogleApi {
  // prevent rate throttling by requesting one at a time
  private static pendingOneAtATime: Promise<any>;
  private static NO_RESULTS_RESPONSE = 'zero_results';
  private static OK_RESPONSE = 'ok';

  public get(key: string) {
    if (key) {
      let fromStorage = this.returnFromLocalStorage(key);
      if (fromStorage) {
        return fromStorage;
      }
      if (GoogleApi.pendingOneAtATime) {
        return this.returnEmpty();
      }
      return this.returnFromGoogle(key);
    }

    return this.returnEmpty();
  }

  public extractLatLon(data) {
    if (data.status && data.status.toLowerCase() == GoogleApi.OK_RESPONSE) {
      if (data.results && data.results[0] && data.results[0].geometry && data.results[0].geometry.location) {
        return data.results[0].geometry.location;
      }
    }
    return null;
  }

  private returnFromLocalStorage(key: string) {
    let fromLocalStorage = localStorage.getItem(key);
    if (fromLocalStorage != null) {
      if (fromLocalStorage == GoogleApi.NO_RESULTS_RESPONSE) {
        return this.returnEmpty();
      } else {
        return new Promise((resolve)=> {
          let value = localStorage.getItem(key);
          try {
            let parsed = JSON.parse(value);
            if (parsed == null) {
              localStorage.removeItem(key);
              resolve(null);
            }
            resolve(parsed);
          } catch (e) {
            localStorage.removeItem(key);
            resolve(null);
          }
        });
      }
    }
    return null;
  }

  private returnFromGoogle(key) {
    let pending = fetch(`${url}${key}`)
        .then((res)=> {
          return res.json();
        })
        .then((data)=> {
          GoogleApi.pendingOneAtATime = null;
          if (this.isOK(data)) {
            let latLon = this.extractLatLon(data);
            localStorage.setItem(key, JSON.stringify(latLon));
            return latLon
          } else if (this.isNoResults(data)) {
            localStorage.setItem(key, GoogleApi.NO_RESULTS_RESPONSE);
            return null;
          } else {
            return null;
          }
        })
        .catch(()=> {
          GoogleApi.pendingOneAtATime = null;
        });
    GoogleApi.pendingOneAtATime = pending;
    return pending;
  }

  private isOK(data) {
    return data.status && data.status.toLowerCase() == GoogleApi.OK_RESPONSE;
  }

  private isNoResults(data) {
    return data.status && data.status.toLowerCase() == GoogleApi.NO_RESULTS_RESPONSE;
  }

  private returnEmpty() {
    return new Promise((resolve)=> {
      resolve(null);
    })
  }
}
