export class Color {
  public get(): {red: number, green: number, blue: number} {
    return {
      red: Math.floor(Math.random() * 255),
      green: Math.floor(Math.random() * 255),
      blue: Math.floor(Math.random() * 255)
    }
  }
}
