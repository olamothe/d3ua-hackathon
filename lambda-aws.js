var dynamo_table = 'npra-devusage';
var dynamo_table_city = 'kinesis-city';
var AWS = require('aws-sdk');
AWS.config.update({region: 'us-east-1'});
var dynamo = new AWS.DynamoDB();
var docClient = new AWS.DynamoDB.DocumentClient();

exports.increment = function (event, context) {
  try {
    console.log('Received event:', JSON.stringify(event, null, 2));
    var currentTimestamp = Math.floor(new Date().getTime() / 1000);

    var totalCount = event.Records.length.toString();
    var searchCount = 0;
    var clickCount = 0;
    var customCount = 0;
    var viewCount = 0;
    var country = "";
    var region = "";
    var city = "";

    console.log("!!!!");

    var getLocalizationInfo = function (message, key) {
      country = message[key].originCountry || 'NOT_AVAILABLE';
      region = message[key].originRegion || 'NOT_AVAILABLE';
      city = message[key].originCity || 'NOT_AVAILABLE';
    }

    event.Records.forEach(function (record) {
      console.log("a");
      var json = new Buffer(record.kinesis.data, 'base64').toString("utf8");
      console.log("JSON", json);
      var message = JSON.parse(json);
      console.log("Message", message);
      if (message.searchEventData) {
        searchCount++;
        getLocalizationInfo(message, 'searchEventData')
      }
      if (message.documentViewData) {
        clickCount++;
        getLocalizationInfo(message, 'documentViewData')
      }
      if (message.customEventData) {
        customCount++;
        getLocalizationInfo(message, 'customEventData')
      }
      if (message.viewEventData) {
        viewCount++;
        getLocalizationInfo(message, 'viewEventData')
      }
    });
    var tableAlreadyUpdated = 0;
    var tableUpdated = function () {
      if (tableAlreadyUpdated == 2) {
        context.done();
      }
    }
    var values = {
      ":t": {"N": totalCount},
      ":s": {"N": searchCount.toString()},
      ":c": {"N": clickCount.toString()},
      ":x": {"N": customCount.toString()},
      ":v": {"N": viewCount.toString()}
    };

    var geolocalization = {
      TableName: dynamo_table_city,
      Item: {
        "Country": country,
        "WorldRegion": region,
        "City": city,
        "timestamp": currentTimestamp.toString()
      }
    };

    console.log("Logging " + totalCount + " events. " + searchCount + " searches, " + clickCount + " clicks, " + customCount + " custom events, " + viewCount + " view events.");
    dynamo.updateItem({
      'TableName': dynamo_table,
      'Key': {"timestamp": {"S": currentTimestamp.toString()}},
      'UpdateExpression': 'ADD TotalCount :t, SearchCount :s, ClickCount :c, CustomCount :x, ViewCount :v',
      'ExpressionAttributeValues': values
    }, function (err, data) {
      if (err) {
        console.log("Error: " + err, data);
      } else {
        console.log("Success", data);
      }
      tableAlreadyUpdated++;
      tableUpdated();

    });

    docClient.put(geolocalization, function (err, data) {
      if (err) {
        console.log("Error geolocalization: " + err, geolocalization);
      } else {
        console.log("Success geolocalization", geolocalization);
      }
      tableAlreadyUpdated++;
      tableUpdated();
    });

  } catch (e) {
    console.log("-----");
    console.log("Error!", e);
    console.log("-----");
  }
};

exports.read = function (event, context) {

  var point_count = 1;
  if (event.n) {
    point_count = parseInt(event.n);
  }

  var timestamp = Math.floor(new Date().getTime() / 1000) - 10;
  var keys = [];
  for (var i = 0; i < point_count; i++) {
    keys.unshift({"timestamp": {"S": timestamp.toString()}});
    timestamp = timestamp - 1;
  }

  var request = {"RequestItems": {}};
  request.RequestItems[dynamo_table] = {"Keys": keys};
  request.RequestItems[dynamo_table_city] = {"Keys": keys};

  var data_points = [];
  var groupedByTimestamp = {};
  console.log("Requesting " + keys.length + " keys.", request, keys);

  dynamo.batchGetItem(request, function (err, data) {
    if (err) {
      console.log("ERROR!", err);
      context.succeed({
        "error": err
      });
    } else {
      console.log("Result: ", data);

      if (data["Responses"][dynamo_table]) {
        var responses = data["Responses"][dynamo_table];
        console.log(responses)

        for (var i = 0; i < responses.length; i++) {
          var response = responses[i];
          var tstamp = response.timestamp.S;
          if (groupedByTimestamp[tstamp] == undefined) {
            groupedByTimestamp[tstamp] = {};
          }

          groupedByTimestamp[tstamp]["totalCount"] = response.TotalCount.N;
          groupedByTimestamp[tstamp]["searchCount"] = response.SearchCount.N;
          groupedByTimestamp[tstamp]["clickCount"] = response.ClickCount.N;
          groupedByTimestamp[tstamp]["customCount"] = response.CustomCount.N;
          groupedByTimestamp[tstamp]["viewCount"] = response.ViewCount.N;
          groupedByTimestamp[tstamp]["timestamp"] = response.timestamp.S;

        }
      }
      if (data["Responses"][dynamo_table_city]) {
        var responses2 = data["Responses"][dynamo_table_city];
        console.log(responses2)

        for (var i = 0; i < responses2.length; i++) {
          var response2 = responses2[i];
          var tstamp = response2.timestamp.S;
          if (groupedByTimestamp[tstamp] == undefined) {
            groupedByTimestamp[tstamp] = {};
          }
          if (groupedByTimestamp[tstamp]["localizations"] == undefined) {
            groupedByTimestamp[tstamp]["localizations"] = [];
          }

          groupedByTimestamp[tstamp]["localizations"].push({
            'city': response2.City.S,
            'country': response2.Country.S,
            'region': response2.WorldRegion.S
          })

          groupedByTimestamp[tstamp]["timestamp"] = response2.timestamp.S;
        }
      }
      var tStampKeys = Object.keys(groupedByTimestamp);
      console.log(groupedByTimestamp, tStampKeys);
      tStampKeys.forEach(function (tStampKey) {
        data_points.push(groupedByTimestamp[tStampKey]);
      })
      context.succeed(data_points);
    }
  });
};
